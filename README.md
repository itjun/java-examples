# java-examples

[![Build Status](https://secure.travis-ci.org/itjun/java-examples.png)](https://travis-ci.org/itjun/java-examples)

1. 该学习项目全程使用 `IntelliJ IDEA` 进行学习和开发，以上说明！
2. 为了尽量确保代码的清晰和可执行性，尽量将每个范例直接放置到一个类的main方法中运行。
3. 代码即注释，相关的说明和注解直接在代码中。
4. 控制台输出统一使用日志log进行输出，请统一在类的头部使用 @Slf4j 注解。